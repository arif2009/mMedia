<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('links'))
{
    function links($links = [])
    {
        foreach($links as $link){
            echo link_tag($link);
        }
    }   
}