<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('Member_model');
    }
    
    public function get_members(){
        $data['members'] = $this->Member_model->get_all_members();
        $result = $this->load->view('vm/memberVM',$data, TRUE);
        echo $result;
    }
    
}
