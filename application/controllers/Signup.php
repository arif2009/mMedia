<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
    
    public $auth;
    public function __construct() {
        parent::__construct();
        $auth = $this->load->library('ion_auth');
        $this->load->model('Religion_model');
        //$this->output->enable_profiler(TRUE);
    }
    
    public function index(){
        $data['religions'] = $this->Religion_model->get_all_religion();
        $data['religion_selection'] = 1;
        $this->load->view("signup/signup", $data);
    }
    
    public function register(){
        $username = $email = $this->input->post('email');
	$password = $this->input->post('password');
        $additional_data = array(
            'first_name'      => $this->input->post('firstName'),
            'last_name'       => $this->input->post('lastName'),
            'display_name'    => $this->input->post('username'),
            'address'         => $this->input->post('address'),
            'address_2'       => $this->input->post('address2'),
            'country_id'      => $this->input->post('country'),
            'zip_code'        => $this->input->post('zip')
        );
        $group = array(MEMBERS); // Sets user to Member.
        
        $this->ion_auth->register($username, $password, $email, $additional_data, $group);
        
        // redirect them to the login page
        redirect('auth/login', 'refresh');
    }
}
