<?php foreach ($members as $member): ?>
    <div class="alert alert-secondary">
        <div class="row">
            <div class="col-md-3 text-center">
                <img src="contents/images/noimage/male-128.png" class="img-thumbnail" alt="Photo" />
            </div>
            <div class="col-md-9 rounded">
                <div>
                    Name : <?=$member['display_name']?> <br/>
                    Age : 22 <br/>
                    Bio : Age and some description
                </div>
                <span class="bottom-right">
                    <?=anchor("member/details/$member[id]", "Details >>", array("class"=>"card-link"))?>
                </span>
            </div>
        </div>
    </div>
<?php endforeach; ?>
