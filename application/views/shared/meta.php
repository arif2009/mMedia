<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Real Estate Marketing, CRM &amp; Transaction Management Software" />
<meta property="og:description" content="RealtyAPX is a complete web-based Real Estate front office, back office and mobile office management software for Brokers and Agents." />
<meta property="og:url" content="http://www.site-needed.com/" />
<meta property="og:site_name" content="RealtyAPX - Real Estate Transaction &amp;  CRM Software" />
<meta property="article:publisher" content="https://www.facebook.com/RealtyAPX" />
<meta property="article:published_time" content="2014-02-17T02:51:55+00:00" />
<meta property="article:modified_time" content="2014-02-25T18:31:09+00:00" />
<meta property="og:image" content="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-frc3/t1/c1.0.160.160/p160x160/1455859_604534996286987_582056805_n.jpg" />

<?php
    $meta_list = array(
        array('name'=>"description", 'content'=>"Description needed."),
        array('name'=>"msapplication-TileColor", 'content'=>"#2d89ef"),
        array('name'=>"msapplication-TileImage", 'content'=>"contents/home/mstile-144x144.png"),
        array('name'=>"theme-color", 'content'=>"#ffffff"),
        array('name'=>"twitter:card", 'content'=>"summary"),
        array('name'=>"twitter:site", 'content'=>"@RealtyAPX"),
        array('name'=>"twitter:domain", 'content'=>"RealtyAPX - Real Estate Transaction &amp;  CRM Software"),
        array('name'=>"twitter:creator", 'content'=>'@RealtyAPX'),
        array('name'=>"google-site-verification", 'content'=>'https://www.google.com/webmasters/tools/dashboard?hl=en&siteUrl=http%3A%2F%2Fwww.realtyapx.com%2F')
    );
    
    echo meta($meta_list);
?>