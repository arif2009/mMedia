<?php
    $css_list = array(
        array(
            'href' => 'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'rel' => 'stylesheet',
            'integrity' => "sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm",
            'crossorigin' => 'anonymous'
        ),
        array('rel' => "stylesheet", 'href' => "bower_components/components-font-awesome/css/fontawesome-all.min.css", 'crossorigin' => 'anonymous'),
        array('rel' => "stylesheet", 'href' => "contents/site.css", 'crossorigin' => 'anonymous')
    );

    echo links($css_list);
?>