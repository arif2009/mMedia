<script type="text/javascript" src="<?=base_url("bower_components/jquery/dist/jquery.min.js")?>" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=base_url("bower_components/jquery-validation/dist/jquery.validate.min.js")?>" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=base_url("bower_components/popper.js/dist/umd/popper.min.js")?>" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= base_url("bower_components/bootstrap/dist/js/bootstrap.min.js")?>" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=base_url("bower_components/blockUI/jquery.blockUI.js")?>" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=base_url("contents/site.js")?>" crossorigin="anonymous"></script>