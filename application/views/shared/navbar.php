<nav class="navbar navbar-expand-md navbar-light bg-light pt-1 pb-1">
    <div class="container">
        <a class="navbar-brand p-0" href="<?=base_url('auth')?>">
            <img src="contents/images/logo/marriage-64.png" alt="LOGO" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <?=anchor('auth', "<i class='fas fa-home text-blue'></i> Home", array("class"=>"nav-link"))?>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);"><i class="fa fa-star text-blue"></i> Favorite <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-envelope text-blue"></i> Inbox</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user text-blue"></i> <?=$name?></a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#"><i class="fa fa-edit text-blue"></i> Edit Profile</a>
                        <a class="dropdown-item" href="#"><i class="fas fa-wrench text-blue"></i> Settings</a>
                        <?=anchor("auth/logout", "<i class='fas fa-lock text-blue'></i> Logout", array("class"=>"dropdown-item"))?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>