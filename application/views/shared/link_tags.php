<?php
    $links = array(
        array('rel'=>'canonical', 'href'=>'http://www.site-needed.com/'),
        array('rel'=>"author", 'href'=>"https://plus.google.com/"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"57x57", 'href'=>"contents/home/apple-touch-icon-57x57.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"60x60", 'href'=>"contents/home/apple-touch-icon-60x60.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"72x72", 'href'=>"contents/home/apple-touch-icon-72x72.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"76x76", 'href'=>"contents/home/apple-touch-icon-76x76.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"114x114", 'href'=>"contents/home/apple-touch-icon-114x114.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"120x120", 'href'=>"contents/home/apple-touch-icon-120x120.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"144x144", 'href'=>"contents/home/apple-touch-icon-144x144.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"152x152", 'href'=>"contents/home/apple-touch-icon-152x152.png"),
        array('rel'=>"apple-touch-icon", 'sizes'=>"180x180", 'href'=>"contents/home/apple-touch-icon-180x180.png"),
        array('rel'=>"icon", 'type'=>"image/png", 'href'=>"contents/images/logo/marriage-16.png", 'sizes'=>"16x16"),
        array('rel'=>"icon", 'type'=>"image/png", 'href'=>"contents/images/logo/marriage-32.png", 'sizes'=>"32x32"),
        array('rel'=>"icon", 'type'=>"image/png", 'href'=>"contents/images/logo/marriage-64.png", 'sizes'=>"64x64"),
        array('rel'=>"icon", 'type'=>"image/png", 'href'=>"contents/images/logo/marriage-128.png", 'sizes'=>"128x128"),
        array('rel'=>'stylesheet', 'id'=>'twentytwelve-fonts-css', 'href'=>'http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&#038;subset=latin,latin-ext','type'=>'text/css','media'=>'all'),
        array('rel'=>"EditURI", 'type'=>"application/rsd+xml", 'title'=>"RSD", 'href'=>"http://sunnahmarriagemedia.org/xmlrpc.php?rsd"),
        array('rel'=>"wlwmanifest", 'type'=>"application/wlwmanifest+xml", 'href'=>"http://sunnahmarriagemedia.org/wp-includes/wlwmanifest.xml"),
        array('rel'=>'shortlink', 'href'=>'http://sunnahmarriagemedia.org/?p=6')
    );
    
    echo links($links);
?>