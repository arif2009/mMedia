<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= doctype() ?>

<html lang="en">
    <head>
        <?php
            $data['title'] = 'member';
            $this->load->View('shared/header',$data);
        ?>
    </head>
    <body>
        <?php $this->load->View('shared/navbar')?>
        
        <main role="main" class="mt-4">
            
            <div class="container">
                <!-- Example row of columns -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="alert alert-secondary">Search</div>
                        <p>Under Construction</p>
                    </div>
                    <div class="col-md-8" id="searchResult"></div>
                </div>

            </div> <!-- /container -->

        </main>
        
        <footer class="container">
            <hr />
            <p>&copy; mMedia 2017-<?= date("Y"); ?></p>
        </footer>
        <?php $this->load->view('shared/js'); ?>
        <script type="text/javascript" src="<?=base_url("contents/js/member.js")?>" crossorigin="anonymous"></script>
    </body>
</html>
