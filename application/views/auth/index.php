<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?=doctype()?>
<html lang="en">
  <head>
    <?php
        $data['title'] = 'Users';
        $this->load->View('shared/header',$data);
    ?>
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <div class="text-center pb-1" style="border-bottom: 1px solid #e5e5e5;">
              <img src="contents/images/logo/marriage-64.png" alt="LOGO" />
            </div>
            <ul class="nav flex-column">
              <li class="nav-item">
                <!--active-->
                <?=anchor('auth', "<i class='fas fa-home'></i> Home", array("class"=>"nav-link"))?>
              </li>
              <li class="nav-item">
                <?=anchor('auth/create_user', "<i class='fas fa-user-plus'></i> ".lang('index_create_user_link'), array("class"=>"nav-link"))?>
              </li>
              <li class="nav-item">
                <?=anchor('auth/create_group', "<i class='fas fa-users'></i> ".lang('index_create_group_link'), array("class"=>"nav-link"))?>
              </li>
              <li class="nav-item">
                <?=anchor("auth/logout", "<i class='fas fa-lock'></i> Logout", array("class"=>"nav-link"))?>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div id="infoMessage"><?php echo $message;?></div>
            <h2 class="text-center">All Users</h2>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search using email, phone or name" aria-label="Search" aria-describedby="btnGroupAddon" />
                <div class="input-group-prepend">
                    <button class="btn btn-primary" id="btnUserSearch" type="button"><i class="fa fa-search"></i></button>
                </div>
            </div>
          <div class="table-responsive">
            <table class="table table-striped table-sm" style="border-top: 2px solid transparent;">
              <thead>
                <tr>
                    <th><?php echo lang('index_fname_th');?></th>
                    <th><?php echo lang('index_lname_th');?></th>
                    <th><?php echo lang('index_email_th');?></th>
                    <th><?php echo lang('index_groups_th');?></th>
                    <th><?php echo lang('index_status_th');?></th>
                    <th><?php echo lang('index_action_th');?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($users as $user):?>
		<tr>
                    <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                    <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                    <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                    <td>
                        <?php foreach ($user->groups as $group):?>
                            <?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                        <?php endforeach?>
                    </td>
                    <td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
                    <td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
		</tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>
      <?php
        $this->load->View('shared/js');
      ?>
  </body>
</html>