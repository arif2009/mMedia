<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?=doctype()?>
<html lang="en">
  <head>
    <?php
        $data['title'] = 'Login';
        $this->load->View('shared/header',$data);
    ?>
  </head>

  <body class="text-center">
    <?php echo form_open("auth/login",'class="form-signin"');?>
      <img class="mb-4" src="<?=base_url('contents/images/logo/marriage-128.png')?>" alt="" width="96" />
      <h1 class="h3 mb-3 font-weight-normal"><?=lang('login_heading')?></h1>
      <p><?php echo lang('login_subheading');?></p>
      <div id="infoMessage" class="alert-warning"><?php echo $message;?></div>
      
      <?php echo lang('login_identity_label', 'identity', 'class="sr-only"');?>
      <?php echo form_input($identity);?>
      
      <?php echo lang('login_password_label', 'password', 'class="sr-only"');?>
      <?php echo form_input($password);?>
      
      <div class="checkbox mb-3">
        <?php echo lang('login_remember_label', 'remember');?>
        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
      </div>
      <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-lg btn-primary btn-block"');?>
      <?=anchor("signup", "Sign Up", array("class"=>"pointer-cursor"))?>
      <a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
      <p class="mt-5 mb-3 text-muted">&copy; mMedia 2017-<?=date("Y");?></p>
    <?php echo form_close();?>
  </body>
</html>
