<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?=doctype()?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php $this->load->view('welcome/welcome_head')?>
    </head>

    <body>
        <div class="logo-fixed"><img src="contents/images/logo/marriage-128.png" alt="Logo" class="logo home" /></div>
        
        <!--Navbar-->
            <?php $this->load->view('welcome/welcome_navbar')?>
        <!--./Navbar-->

        <!--HOME-->
            <?php $this->load->view('welcome/welcome_home')?>
        <!--./HOME-->
    
        <!--FEATURES-->
            <?php $this->load->view('welcome/welcome_features')?>
        <!--./FEATURES-->
        
        <!--ABOUT-->
            <?php $this->load->view('welcome/welcome_about')?>
        <!--./ABOUT-->
        
        <!--BLOG-->
            <?php $this->load->view('welcome/welcome_blog')?>
        <!--./BLOG-->
            
        <!--CONTACT-->
            <?php $this->load->view('welcome/welcome_contact')?>
        <!--./CONTACT-->
        
    <script type="text/javascript" src="<?=base_url("bower_components/jquery/dist/jquery.min.js")?>" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= base_url("bower_components/video.js/dist/video-js/video.js")?>"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>