<?php $this->load->view('shared/meta'); ?>

<title>Title Needed</title>

<?php $this->load->view('shared/link_tags'); ?>

<?php
    $css_list = array(
        array(
            'rel' => "stylesheet", 
            'href' => "//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", 
            'rel' => "stylesheet",
            'integrity' => "sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u",
            'crossorigin'=> "anonymous"
        ),
        array(
            'rel' => "stylesheet", 
            'href' => "contents/home/css/styles.css", 
            'crossorigin' => 'anonymous'
        ),
        array(
            'rel' => "stylesheet", 
            'href' => "bower_components/video.js/dist/video-js/video-js.min.css", 
            'crossorigin'=> "anonymous"
        )
    );
    
    echo links($css_list);
?>