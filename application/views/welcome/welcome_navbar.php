<nav class="navbar navbar-default navbar-fixed-top navbar-cloud">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="/login/logout" class="btn btn-sm btn-success hidden-md hidden-lg pull-right" style="margin-top: 25px; margin-right: 15px;">Login</a>
        </div>
        <div id="navbar" class="hidden-xs hidden-sm">
            <ul class="nav navbar-nav pull-right">
                <li><a href="#home">Home</a></li>
                <li><a href="#features">Features</a></li>
                <li><a href="#about">About Us</a></li>
                <li><a href="#blog">Blog</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><?=anchor("signup", "Sign Up", array("class"=>"pointer-cursor"))?></li>
                <li><?=anchor("auth", "Login", array("class"=>"btn btn-success"))?></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<a class="anchor" name="home" id="home" style="top: -110px;"></a>

<div class="feature-list container-fluid hidden-md hidden-lg blueback">
    <div class="row">
        <div class="col-xs-12">
            <div class="complete-solution">
                <h4 class="margtop20">Complete Real Estate Solution:</h4>
                <ul>
                    <li><a href="#home">Home</a></li>
                    <li><a href="#features">Features</a></li>
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#blog">Blog</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="/signup" style="cursor: pointer;">Sign Up</a></li>
                </ul>
            </div>
            <div class="video-box">
                <video id="example_video_id_1293585824" poster="contents/home/images/video-poster.jpg" class="video-js vjs-default-skin" controls=controls preload="auto" data-setup='[]'>
                    <source src="contents/home/video/Facebook_23.mp4" type='video/mp4' />
                </video><!--./video mobile-->
            </div>

        </div>
    </div>
</div><!--./mobile-bullets-->