<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?=doctype()?>
<html lang="en">
  <head>
    <?php
        $data['title'] = 'Sign Up';
        $this->load->View('shared/header',$data);
    ?>
  </head>
  <body>
      
    <main role="main">
      <div class="container">
        <div class="py-4 text-center">
          <img class="d-block mx-auto mb-3" src="contents/images/logo/marriage-128.png" alt="" width="96" />
          <h2>Sign Up</h2>
          <p class="lead">Below is an example form built entirely with Bootstrap's form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>
        </div>
        <div class="row">
            <div class="col-12">
                <form id="signupForm" name="signupForm" action="Signup/register" method="post" class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">First name <span class="text-muted">(Required)</span></label>
                            <input type="text" id="firstName" name="firstName" class="form-control" placeholder="First name">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Last name <span class="text-muted">(Required)</span></label>
                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last name">
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="username">Display Name / Nick Name </label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Name" />
                    </div>

                    <div class="mb-3">
                        <label for="email">Email <span class="text-muted">(Required)</span></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
                    </div>

                    <div class="mb-3">
                        <label for="address">Address <span class="text-muted">(Required)</span></label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St">
                    </div>

                    <div class="mb-3">
                        <label for="address2">Address 2 </label>
                        <input type="text" class="form-control" id="address2" name="address2" placeholder="Apartment or suite">
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="country">Country <span class="text-muted">(Required)</span></label>
                            <select class="custom-select d-block w-100" name="country" id="country">
                                <option value="">Choose...</option>
                                <option value="1">Bangladesh</option>
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="zip">Zip</label>
                            <input type="text" class="form-control" id="zip" name="zip" placeholder="Zip code">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="password">Password <span class="text-muted">(Required)</span></label>
                            <input type="password" id="password" name="password" class="form-control" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="confirm_password">Confirm password <span class="text-muted">(Required)</span></label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" />
                        </div>
                    </div>
                    <hr class="mb-4">
                    <div class="row">
                        <div class="col-6">
                            <h5>Religion</h5>
                            
                            <div class="d-block">
                                <?php foreach ($religions as $religion): ?>
                                    <div class="custom-control custom-radio">
                                        <input id="<?=$religion['key']?>" name="religion" type="radio" value="<?=$religion['id']?>" class="custom-control-input" <?=$religion_selection==$religion['id']?'checked required':''?>>
                                        <label class="custom-control-label" for="<?=$religion['key']?>"><?=$religion['value']?></label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-6">
                            <h5>Gender</h5>

                            <div class="d-block">
                                <div class="custom-control custom-radio">
                                    <input id="male" name="gender" type="radio" class="custom-control-input" value="1" checked required>
                                    <label class="custom-control-label" for="male">Male</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="female" name="gender" type="radio" class="custom-control-input" value="0">
                                    <label class="custom-control-label" for="female">Female</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <hr class="mb-4">
                    <input type="checkbox" class="checkbox" id="agree" name="agree" /> <label for="agree">Please agree to our policy <span class="text-muted">(Required)</span> </label>
                    <button disabled="true" class="btn btn-primary btn-lg btn-block" id="btnSubmit" name="btnSubmit" type="submit"><i class="far fa-save"></i> &nbsp; Continue to checkout &nbsp;<i class="fas fa-arrow-right"></i></button>
                </form>
            </div>
        </div>
      </div> <!-- /container -->
    </main>
    
    <footer class="container">
      <hr />
      <p>&copy; mMedia 2017-<?=date("Y");?></p>
    </footer>
    <?php $this->load->view('shared/js');?>
    <script type="text/javascript" src="<?=base_url("contents/js/validate.js")?>" crossorigin="anonymous"></script>
  </body>
</html>