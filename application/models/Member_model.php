<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Member_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function get_all_members(){
        $members = $this->ion_auth->users(MEMBERS)->result_array();
        return $members;
    }
}
