<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Religion_model extends CI_Model {
    
    public function get_all_religion() {
        $query = $this->db->get('religion');
        return $query->result_array();
    }
}
