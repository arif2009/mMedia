###################<br/>
Marriage Media<br/>
###################<br/>

## Development Tools & Technology :
* [CodeIgniter v3.1.7 Framework](https://www.codeigniter.com/)
* [Ion Auth](http://benedmunds.com/ion_auth/)
* [MariaDB 10.1.30](https://www.mysql.com/)
* [bootstrap-4](https://getbootstrap.com/)
* [AJAX & JQuery](https://jquery.com/)


## Installation

```bash
# HTTPS clone URL
$ git clone https://github.com/arif2009/mMedia.git

# SSH clone URL
$ git clone git@github.com:arif2009/mMedia.git
```

You need to have [Git](https://git-scm.com/) and [Node.js](https://nodejs.org/en/) installed on your machine before running the followings:

```bash
$ npm install -g bower

$ cd /path/to/mMedia
$ bower i

# Build and run the solution
```
For more information please visit [bower.io](http://bower.io/)

## License
This application is released under the [MIT](http://www.opensource.org/licenses/MIT) License.

Copyright (c) 2018 [Arifur Rahman.](http://arifur-rahman-sazal.blogspot.com/)
