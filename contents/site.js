'use strict';
$(document).ajaxStart(function () {
  $.blockUI({ message: '<div class="font-weight-bold"><i class="fas fa-sync fa-spin"></i> Just a moment...</div>' });
}).ajaxStop($.unblockUI);