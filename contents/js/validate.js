'use strict';

(function () {
    // validate signup form on keyup and submit
    $("#signupForm").validate({
        rules: {
            firstName: "required",
            lastName: "required",
            username: {
                required: false,
                minlength: 2
            },
            address: "required",
            country: "required",
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            agree: "required"
        },
        messages: {
            firstName: "Please enter your firstname",
            lastName: "Please enter your lastname",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            address: "Please enter your address",
            country: "Select your country",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"
        }
    });
    
    // propose username by combining first- and lastname
    $("#username").focus(function () {
        var firstname = $("#firstName").val();
        var lastname = $("#lastName").val();
        if (firstname && lastname && !this.value) {
            this.value = firstname + " " + lastname;
        }
    });
    
    $("#agree").change(function() {
        $("#btnSubmit").attr("disabled", !this.checked);
    });

})();